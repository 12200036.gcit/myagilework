// const express = require("express")
// const userRouter = require('./routes/userRoutes')
// const viewRouter = require('./routes/viewRoutes')
// const path = require('path')

// const app = express()
// app.use(express.json())
// app.use('/api/v1/users', userRouter)
// app.use('/', viewRouter)

// app.use(express.static(path.join(__dirname, 'views')))

// module.exports = app

// // Starting the server on the port 4001
// const port = 4001
// app.listen(port, () => {
//     console.log(`App running on port ${port} ..`)
// })


const express = require("express")
const path=require('path')
const app = express()
const userRouter =require('./routes/userRoutes')
const viewRouter=require('./routes/viewRoutes')
const cookieParser = require('cookie-parser')
app.use(cookieParser())

app.use(express.json())
app.use('/api/v1/Users',userRouter)
app.use('/',viewRouter)

app.use(express.static(path.join(__dirname,'views')))

module.exports=app